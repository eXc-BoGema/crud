﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Interfaces;
using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.DAL.Repositories
{
    public class BookRepository : IRepository<Book>
    {
        private CrudContext _dbContext;
        public BookRepository(CrudContext context)
        {
            _dbContext = context;
        }

        public void Create(Book book)
        {
            _dbContext.Books.Add(book);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Book book = _dbContext.Books.Where(x => x.Id == id).FirstOrDefault();
            if (book != null)
            {
                _dbContext.Books.Remove(book);
                _dbContext.SaveChanges();
            }
        }

        public Book Get(int id)
        {
            return _dbContext.Books.Where(x => x.Id == id).First();
        }

        public List<Book> GetAll()
        {
            return _dbContext.Books.ToList();
        }

        public void Update(Book book)
        {
            Book temp = _dbContext.Books.Where(x => x.Id == book.Id).FirstOrDefault();
            temp.Name = book.Name;
            temp.Authors.Clear();
            temp.Authors = book.Authors;

            _dbContext.SaveChanges();
        }
        public List<Author> GetAuthors()
        {
            return _dbContext.Authors.ToList();
        }


    }
}
