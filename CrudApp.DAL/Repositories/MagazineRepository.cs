﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Interfaces;
using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.DAL.Repositories
{
    public class MagazineRepository : IRepository<Magazine>
    {
        private CrudContext _dbContext;
        public MagazineRepository(CrudContext context)
        {
            _dbContext = context;
        }

        public void Create(Magazine magazine)
        {
            _dbContext.Magazines.Add(magazine);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Magazine magazine = _dbContext.Magazines.Where(x => x.Id == id).FirstOrDefault();
            _dbContext.Magazines.Remove(magazine);

            _dbContext.SaveChanges();
        }

        public Magazine Get(int id)
        {
            return _dbContext.Magazines.Where(x => x.Id == id).First();
        }

        public List<Magazine> GetAll()
        {
            return _dbContext.Magazines.ToList();
        }

        public void Update(Magazine magazine)
        {
            Magazine temp = _dbContext.Magazines.Where(x => x.Id == magazine.Id).FirstOrDefault();
            temp.Name = magazine.Name;
            temp.Articles.Clear();
            temp.Articles = magazine.Articles;

            _dbContext.SaveChanges();
        }

        public List<Article> GetArticles()
        {
            return _dbContext.Articles.ToList();
        }

    }
}
