﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Interfaces;
using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.DAL.Repositories
{
    public class ArticleRepository:IRepository<Article>
    {
        private CrudContext _dbContext;
        public ArticleRepository(CrudContext context)
        {
            _dbContext = context;
        }

        public void Create(Article article)
        {
            _dbContext.Articles.Add(article);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Article article = _dbContext.Articles.Where(x => x.Id == id).FirstOrDefault();
            if (article != null)
            {
                _dbContext.Articles.Remove(article);
                _dbContext.SaveChanges();
            }
        }

        public Article Get(int id)
        {
            return _dbContext.Articles.Where(x => x.Id == id).First();
        }

        public List<Article> GetAll()
        {
            return _dbContext.Articles.ToList();
        }

        public void Update(Article article)
        {
            Article temp = _dbContext.Articles.Where(x => x.Id == article.Id).FirstOrDefault();
            temp.Name = article.Name;
            temp.Authors.Clear();
            temp.Authors = article.Authors;

            _dbContext.SaveChanges();
        }
        public List<Author> GetAuthors()
        {
            return _dbContext.Authors.ToList();
        }

    }
}
