﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Interfaces;
using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.DAL.Repositories
{
    public class AuthorRepository:IRepository<Author>
    {
        private CrudContext _dbContext;
        public AuthorRepository(CrudContext context)
        {
            _dbContext = context;
        }

        public void Create(Author author)
        {
            _dbContext.Authors.Add(author);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Author autor = _dbContext.Authors.Where(x => x.Id == id).FirstOrDefault();
            _dbContext.Authors.Remove(autor);

            _dbContext.SaveChanges();
        }

        public Author Get(int id)
        {
            return _dbContext.Authors.Where(x => x.Id == id).First();
        }

        public List<Author> GetAll()
        {
            return _dbContext.Authors.ToList();
        }

        public void Update(Author author)
        {
            Author temp = _dbContext.Authors.Where(x => x.Id == author.Id).FirstOrDefault();
            temp.Name = author.Name;
            temp.Surname = author.Surname;
            _dbContext.SaveChanges();
        }
    }
}
