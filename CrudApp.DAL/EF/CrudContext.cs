﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrudApp.Domain.Entities;

namespace CrudApp.DAL.EF
{
    public class CrudContext : DbContext
    {
        static CrudContext()
        {
            Database.SetInitializer<CrudContext>(new CrudDbInitializer());
        }
        public CrudContext(string connectionString)
            : base(connectionString)
        {
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
    }
}
