﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.DAL.EF
{
    public class CrudDbInitializer : DropCreateDatabaseAlways<CrudContext>
    {
        protected override void Seed(CrudContext db)
        {
            db.Authors.Add(new Author() { Name = "Petya", Surname = "Petrovich" });
            db.Authors.Add(new Author() { Name = "Vasya", Surname = "Vasiljevich" });
            db.Authors.Add(new Author() { Name = "Mawa", Surname = "Rasputina" });

            db.Books.Add(new Book() { Name = "Book 1", Authors = new List<Author>() });
            db.Books.Add(new Book() { Name = "Book 2", Authors = new List<Author>() });
            db.Books.Add(new Book() { Name = "Book 3", Authors = new List<Author>() });

            db.Articles.Add(new Article() { Name = "Article 1", Authors = new List<Author>() });
            db.Articles.Add(new Article() { Name = "Article 2", Authors = new List<Author>() });
            db.Articles.Add(new Article() { Name = "Article 3", Authors = new List<Author>() });

            db.Magazines.Add(new Magazine() { Name = "Magazine 1", Articles = new List<Article>() });
            db.Magazines.Add(new Magazine() { Name = "Magazine 2", Articles = new List<Article>() });
            db.Magazines.Add(new Magazine() { Name = "Magazine 3", Articles = new List<Article>() });

            base.Seed(db);
        }
    }
}
