﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.MagazineViewModels
{
    public class MagazineView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Articles { get; set; }
    }
}
