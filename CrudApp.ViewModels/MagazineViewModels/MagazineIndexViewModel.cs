﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.MagazineViewModels
{
    public class MagazineIndexViewModel
    {
        public List<MagazineView> Magazines { get; set; }
        public List<Article> Articles { get; set; }
    }
}
