﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.ArticleViewModels
{
    public class ArticleIndexViewModel
    {
        public List<ArticleView> Articles { get; set; }
        public List<Author> Authors { get; set; }
    }
}
