﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.BookViewModels
{
    public class BookView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Authors { get; set; }
    }
}
