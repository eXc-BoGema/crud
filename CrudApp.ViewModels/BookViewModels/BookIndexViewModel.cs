﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.BookViewModels
{
    public class BookIndexViewModel
    {
        public List<BookView> Books { get; set; }
        public List<Author> Authors { get; set; }
    }
}
