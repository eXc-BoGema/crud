﻿using CrudApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.ViewModels.AuthorViewModels
{
    public class AuthorIndexViewModel
    {
        public List<Author> Authors { get; set; }
    }
}
