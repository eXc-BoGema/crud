﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Repositories;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.BookViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.BLL.Servies
{
    public class BookService
    {
        private BookRepository _bookRepository;

        public BookService()
        {
            _bookRepository = new BookRepository(new CrudContext("DefaultString"));
        }

        public void FillIndexView(BookIndexViewModel view)
        {
            view.Books = new List<BookView>();
            var tempBooks = _bookRepository.GetAll();

            //authors to []string
            foreach (var x in tempBooks)
            {
                var newBook = new BookView() { Id = x.Id, Name = x.Name, Authors = new List<string>() };
                foreach (var author in x.Authors)
                {
                    newBook.Authors.Add(author.Name + " " + author.Surname);
                }
                view.Books.Add(newBook);
            }

            view.Authors = _bookRepository.GetAuthors();
        }
        public void Create(Book book)
        {
            _bookRepository.Create(book);
        }
        public void Update(Book book)
        {
            _bookRepository.Update(book);
        }
        public void Delete(int id)
        {
            _bookRepository.Delete(id);
        }

        public List<Book> GetBooks()
        {
            return _bookRepository.GetAll();
        }

    }
}
  