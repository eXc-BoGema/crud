﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Repositories;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.AuthorViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.BLL.Servies
{
    public class AuthorService
    {
        private AuthorRepository _authorRepository;
        public AuthorService()
        {
            _authorRepository = new AuthorRepository(new CrudContext("DefaultString"));
        }

        public void FillIndexView(AuthorIndexViewModel view)
        {
            view.Authors = _authorRepository.GetAll();
        }
        public void Create(Author author)
        {
            _authorRepository.Create(author);
        }
        public void Update(Author author)
        {
            _authorRepository.Update(author);
        }
        public void Delete(int id)
        {
            _authorRepository.Delete(id);
        }
    }
}
