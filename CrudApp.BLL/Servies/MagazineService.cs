﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Repositories;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.MagazineViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.BLL.Servies
{
    public class MagazineService
    {
        private MagazineRepository _magazineRepository;

        public MagazineService()
        {
            _magazineRepository = new MagazineRepository(new CrudContext("DefaultString"));
        }

        public void FillIndexView(MagazineIndexViewModel view)
        {
            view.Magazines = new List<MagazineView>();
            var tempMagazines = _magazineRepository.GetAll();

            //authors to []string
            foreach (var x in tempMagazines)
            {
                var newMagazine = new MagazineView() { Id = x.Id, Name = x.Name, Articles = new List<string>() };
                foreach (var article in x.Articles)
                {
                    newMagazine.Articles.Add(article.Name);
                }
                view.Magazines.Add(newMagazine);
            }

            view.Articles = _magazineRepository.GetArticles();
        }
        public void Create(Magazine magazine)
        {
            _magazineRepository.Create(magazine);
        }
        public void Update(Magazine magazine)
        {
            _magazineRepository.Update(magazine);
        }
        public void Delete(int id)
        {
            _magazineRepository.Delete(id);
        }
    }
}
