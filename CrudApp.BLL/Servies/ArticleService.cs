﻿using CrudApp.DAL.EF;
using CrudApp.DAL.Repositories;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.ArticleViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.BLL.Servies
{
    public class ArticleService
    {
        private ArticleRepository _articleRepository;

        public ArticleService()
        {
            _articleRepository = new ArticleRepository(new CrudContext("DefaultString"));
        }

        public void FillIndexView(ArticleIndexViewModel view)
        {
            view.Articles = new List<ArticleView>();
            var tempArticles = _articleRepository.GetAll();
            foreach (var x in tempArticles)
            {
                var newBook = new ArticleView() { Id = x.Id, Name = x.Name, Authors = new List<string>() };
                foreach (var author in x.Authors)
                {
                    newBook.Authors.Add(author.Name + " " + author.Surname);
                }
                view.Articles.Add(newBook);
            }

            view.Authors = _articleRepository.GetAuthors();
        }
        public void Create(Article article)
        {
            _articleRepository.Create(article);
        }
        public void Update(Article article)
        {
            _articleRepository.Update(article);
        }
        public void Delete(int id)
        {
            _articleRepository.Delete(id);
        }
    }
}
