﻿using CrudApp.Domain.Entities;
using CrudApp.ViewModels.ArticleViewModels;
using CrudApp.ViewModels.BookViewModels;
using CrudApp.ViewModels.MagazineViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.BLL.Converter
{
    public class Converter
    {
        public static Book ConvertBookViewToBook(BookView bookView,List<Author> Authors)
        {
            Book tempBook = new Book() { Id = bookView.Id, Name = bookView.Name, Authors = new List<Author>() };
            if (bookView.Authors != null)
            {
                foreach (var author in bookView.Authors)
                {
                    var temp = author.Split(new char[] { ' ' });

                    tempBook.Authors.Add(Authors.Where(x => x.Name == temp[0] && x.Surname == temp[1]).FirstOrDefault());
                }
            }
            return tempBook;
        }

        public static Article ConvertArticleViewToArticle(ArticleView articleView, List<Author> Authors)
        {
            Article tempArticle = new Article() { Id = articleView.Id, Name = articleView.Name, Authors = new List<Author>() };
            if (articleView.Authors != null)
            {
                foreach (var author in articleView.Authors)
                {
                    var temp = author.Split(new char[] { ' ' });

                    tempArticle.Authors.Add(Authors.Where(x => x.Name == temp[0] && x.Surname == temp[1]).FirstOrDefault());
                }
            }
            return tempArticle;
        }

        public static Magazine ConvertMagazineViewToMagazine(MagazineView magazineView, List<Article> Articles)
        {

            Magazine tempMagazine = new Magazine() { Id = magazineView.Id, Name = magazineView.Name, Articles = new List<Article>() };
            if (magazineView.Articles != null)
            {
                foreach (var article in magazineView.Articles)
                {
                    tempMagazine.Articles.Add(Articles.Where(x => x.Name == article).FirstOrDefault());
                }
            }
            return tempMagazine;
        }




    }
}
