﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace CrudApp.Domain.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual List<Book> Books { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual List<Article> Articles { get; set; }
    }
}
