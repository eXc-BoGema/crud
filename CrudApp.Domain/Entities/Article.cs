﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CrudApp.Domain.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Author> Authors { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual List<Magazine> Magazines { get; set; }
    }
}
