﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudApp.Domain.Entities
{
    public class Magazine
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Article> Articles { get; set; }
    }
}
