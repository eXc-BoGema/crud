﻿using AutoMapper;
using CrudApp.BLL.Converter;
using CrudApp.BLL.Servies;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.BookViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudApp.WEB.Controllers
{
    public class BookController : Controller
    {
        private BookService _bookService;


        public BookController()
        {
            _bookService = new BookService();
            Mapper.Initialize(cfg => cfg.CreateMap<Book, BookView>()
            .ForMember(x => x.Authors, o => o.MapFrom(c => c.Authors.Select(m => m.Name + " " + m.Surname).ToArray())));

            //Mapper.Initialize(cfg => cfg.CreateMap<BookView, Book>()
            //.ForMember(x => x.Authors[].Name, o => o.MapFrom(c => c.Authors.Select(m => m.Split(' ').ToList()))));
        }


        public ActionResult Index()
        {
            var view = new BookIndexViewModel();
            //_bookService.FillIndexView(view);
            var books = Mapper.Map<IEnumerable<Book>, List<BookView>>(_bookService.GetBooks());
            view.Books = books;

            return View(view);
        }

        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            var view = new BookIndexViewModel();
            _bookService.FillIndexView(view);

            return Json(view.Books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, BookView bookView)
        {
            var view = new BookIndexViewModel();
            _bookService.FillIndexView(view);

            Book book = Converter.ConvertBookViewToBook(bookView, view.Authors);
            if (book != null && ModelState.IsValid)
            {
                _bookService.Create(book);
            }

            return Json(new[] { bookView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, BookView bookView)
        {
            var view = new BookIndexViewModel();
            _bookService.FillIndexView(view);

            Book book = Converter.ConvertBookViewToBook(bookView,view.Authors);


            if (bookView != null && ModelState.IsValid)
            {
                _bookService.Update(book);
            }

            return Json(new[] { bookView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, Book book)
        {
            if (book != null)
            {
                _bookService.Delete(book.Id);
            }
            return Json(new[] { book }.ToDataSourceResult(request, ModelState));
        }

        //for partial view (print authors in view)
        public ActionResult GetAuthors([DataSourceRequest] DataSourceRequest request)
        {
            var view = new BookIndexViewModel();
            _bookService.FillIndexView(view);

            List<string> sendingItems = new List<string>();
            foreach (var x in view.Authors)
            {
                sendingItems.Add(x.Name + " " + x.Surname);
            }
            return Json(sendingItems, JsonRequestBehavior.AllowGet);
        }
    }
}