﻿using CrudApp.BLL.Servies;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.AuthorViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudApp.WEB.Controllers
{
    public class AuthorController : Controller
    {
        private AuthorService _authorService;
        public AuthorController()
        {
            _authorService = new AuthorService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditingInline_Read([DataSourceRequest] DataSourceRequest request)
        {
            AuthorIndexViewModel view = new AuthorIndexViewModel();
            _authorService.FillIndexView(view);

            return Json(view.Authors.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Update([DataSourceRequest] DataSourceRequest request, Author author)
        {
            if (author != null && ModelState.IsValid)
            {
                    _authorService.Update(author);
            }

            return Json(new[] { author }.ToDataSourceResult(request,ModelState), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Create([DataSourceRequest] DataSourceRequest request,Author author)
        {
            if (author != null && ModelState.IsValid)
                    _authorService.Create(author);

            return Json(new[] { author }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Destroy([DataSourceRequest] DataSourceRequest request, Author author)
        {
            _authorService.Delete(author.Id);

            return Json(new[] {author }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
    }
}