﻿using CrudApp.BLL.Converter;
using CrudApp.BLL.Servies;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.MagazineViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudApp.WEB.Controllers
{
    public class MagazineController : Controller
    {
        private MagazineService _magazineService;
        public MagazineController()
        {
            _magazineService = new MagazineService();
        }


        public ActionResult Index()
        {
            var view = new MagazineIndexViewModel();
            _magazineService.FillIndexView(view);

            return View(view);
        }

        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            var view = new MagazineIndexViewModel();
            _magazineService.FillIndexView(view);

            return Json(view.Magazines.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, MagazineView magazineView)
        {
            var view = new MagazineIndexViewModel();
            _magazineService.FillIndexView(view);

            Magazine magazine = Converter.ConvertMagazineViewToMagazine(magazineView,view.Articles);

            if (magazine != null && ModelState.IsValid)
            {
                _magazineService.Create(magazine);
            }

            return Json(new[] { magazineView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, MagazineView magazineView)
        {
            var view = new MagazineIndexViewModel();
            _magazineService.FillIndexView(view);

            Magazine magazine = Converter.ConvertMagazineViewToMagazine(magazineView, view.Articles);


            if (magazineView != null && ModelState.IsValid)
            {
                _magazineService.Update(magazine);
            }

            return Json(new[] { magazineView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, Book book)
        {
            if (book != null)
            {
                _magazineService.Delete(book.Id);
            }

            return Json(new[] { book }.ToDataSourceResult(request, ModelState));
        }

        //for partial view (print authors in view)
        public ActionResult GetArticles([DataSourceRequest] DataSourceRequest request)
        {
            var view = new MagazineIndexViewModel();
            _magazineService.FillIndexView(view);

            List<string> sendingItems = new List<string>();
            foreach (var x in view.Articles)
            {
                sendingItems.Add(x.Name);
            }
            return Json(sendingItems, JsonRequestBehavior.AllowGet);
        }

    }
}