﻿using CrudApp.BLL.Converter;
using CrudApp.BLL.Servies;
using CrudApp.Domain.Entities;
using CrudApp.ViewModels.ArticleViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudApp.WEB.Controllers
{
    public class ArticleController : Controller
    {
        private ArticleService _articleService;
        public ArticleController()
        {
            _articleService = new ArticleService();
        }


        public ActionResult Index()
        {
            var view = new ArticleIndexViewModel();
            _articleService.FillIndexView(view);

            return View(view);
        }

        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            var view = new ArticleIndexViewModel();
            _articleService.FillIndexView(view);

            return Json(view.Articles.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, ArticleView articleView)
        {
            var view = new ArticleIndexViewModel();
            _articleService.FillIndexView(view);

            Article article = Converter.ConvertArticleViewToArticle(articleView, view.Authors);

            if (article != null && ModelState.IsValid)
            {
                _articleService.Create(article);
            }

            return Json(new[] { articleView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, ArticleView articleView)
        {
            var view = new ArticleIndexViewModel();
            _articleService.FillIndexView(view);

            Article article = Converter.ConvertArticleViewToArticle(articleView,view.Authors);


            if (articleView != null && ModelState.IsValid)
            {
                _articleService.Update(article);
            }

            return Json(new[] { articleView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, Article article)
        {
            if (article != null)
            {
                _articleService.Delete(article.Id);
            }

            return Json(new[] { article }.ToDataSourceResult(request, ModelState));
        }

        //for partial view (print authors in view)
        public ActionResult GetAuthors([DataSourceRequest] DataSourceRequest request)
        {
            var view = new ArticleIndexViewModel();
            _articleService.FillIndexView(view);

            List<string> sendingItems = new List<string>();
            foreach (var x in view.Authors)
            {
                sendingItems.Add(x.Name + " " + x.Surname);
            }
            return Json(sendingItems, JsonRequestBehavior.AllowGet);
        }


    }
}